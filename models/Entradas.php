<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $texto
 * @property string|null $fecha
 * @property string|null $foto
 */
class Entradas extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['texto'], 'string'],
            [['fecha'], 'safe'],
            [['titulo', 'foto'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
        ];
    }

    public function getFotoFinal(): string {
        // compruebo si no tengo 
        if (empty($this->foto)) {
            return \yii\helpers\Html::img(
                            "@web/imgs/anonimo.png",
                            [
                                'class' => 'img-thumbnail d-block mx-auto my-2', // aspecto
                            ]
            );
        }

        return \yii\helpers\Html::img(
                        "@web/imgs/$this->foto",
                        [
                            'class' => 'img-thumbnail d-block mx-auto my-2', // aspecto
                        ]
        );
    }

}
