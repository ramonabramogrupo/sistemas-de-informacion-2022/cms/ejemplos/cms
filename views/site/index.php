<?php

use app\models\Entradas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entradas-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'texto:ntext',
            'fecha',
            // 'foto',
            [
                'attribute' => 'foto',
                'format' => 'raw', // para dibuje imagen
                'value' => function ($model) {
                    return $model->fotoFinal;
                },
                'options' => [
                    'class' => 'col-3',
                ]
            ],
            
        ],
    ]); ?>


</div>
