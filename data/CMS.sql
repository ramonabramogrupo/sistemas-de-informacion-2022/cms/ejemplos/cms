﻿DROP DATABASE IF EXISTS cms;
CREATE DATABASE cms;
USE cms;

CREATE TABLE entradas(
  id int AUTO_INCREMENT,
  titulo varchar(100),
  texto text,
  fecha datetime DEFAULT CURRENT_TIMESTAMP,
  foto varchar(100),
  PRIMARY KEY(id)
);


